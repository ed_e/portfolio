import { AppBar, Container, Toolbar, Typography } from '@mui/material';
import * as React from 'react';

export default function PortfolioAppBar() {
    return <AppBar position="sticky">
        <Toolbar disableGutters>
            <Container maxWidth="xl">
                <Typography variant="h6" color="inherit">
                    Ed Englefield
                </Typography>
            </Container>
        </Toolbar>
    </AppBar>;
}