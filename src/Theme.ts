// Serif theme with light and dark mode
import { createTheme } from "@mui/material";

const muiTheme = createTheme({
    palette: {
        primary: {
            main: "#000",
            contrastText: "#fff",
        },
        secondary: {
            main: "#fff",
            contrastText: "#000",
        },
        mode: "dark",
    },
    typography: {
        fontFamily: "'Playfair Display', serif",
    },
});

export default muiTheme;