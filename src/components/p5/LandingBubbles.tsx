/*
Background bubles and graphics on the landing page
*/

import React from "react";
import Sketch from "react-p5";
import p5Types from "p5"; //Import this for typechecking and intellisense
import Particle from "./sprites/Particle";

interface ComponentProps {
	//Your component props
}

let Particles: Particle[] = [];
  
const LandingBubbles: React.FC<ComponentProps> = (props: ComponentProps) => {
	

	//See annotations in JS for more information
	const setup = (p5: p5Types, canvasParentRef: Element) => {
        const appRoot = document.getElementById("app-root")!;
		p5.createCanvas(appRoot.offsetWidth, appRoot.offsetHeight).parent(canvasParentRef);

        // Generate particles
        for (let i = 0; i < 300; i++) {
            const newParticle = new Particle({
                p5: p5,
                startX: p5.random(p5.width),
                startY: p5.random(p5.height),
                speed: p5.random(0.5, 2),
                size: p5.random(10, 80),
                dispose: () => {
                    Particles.splice(Particles.indexOf(newParticle), 1);
                }
            });
            Particles.push(newParticle);
        };
	};

	const draw = (p5: p5Types) => {
        // Set background colour to dark magenta
        p5.background(20, 0, 50);
        p5.noStroke();
		// Draw all particles
        Particles.forEach((particle) => {
            particle.draw({
                boundX: p5.width,
                boundY: p5.height,
            });
        });
	};

    const windowResized = (p5: p5Types) => {
        // Set to height and width off #app-root
        const appRoot = document.getElementById("app-root")!;
        p5.resizeCanvas(appRoot.offsetWidth, appRoot.offsetHeight);
    }

	return <Sketch setup={setup} draw={draw} windowResized={windowResized} />;
};

export default LandingBubbles;