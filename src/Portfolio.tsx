// The root class for my portfolio site
import { Box, Container, Grid, Typography } from '@mui/material';
import * as React from 'react';
import PortfolioAppBar from './components/appbar/PortfolioAppBar';
import LandingBubbles from './components/p5/LandingBubbles';

export default class Portfolio extends React.Component {
    render() {
        return (
            <Box>
                <LandingBubbles />
                <PortfolioAppBar />
                    <Grid container
                        spacing={0}
                        direction="column"
                        justifyContent="center"
                        sx={{ minHeight: '90vh'}}>
                        <Grid item xs={3}>
                            <Container maxWidth="sm">
                                <Typography variant="h4" gutterBottom>
                                    Portfolio
                                </Typography>
                                <Typography variant="body1" gutterBottom>
                                    This is my portfolio site.
                                </Typography>
                            </Container>
                            
                        </Grid>
                    </Grid>
            </Box>
        );
    }
}