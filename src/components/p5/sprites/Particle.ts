import p5Types from "p5";

// For landing page circles

export interface particleProps {
    p5: p5Types;
    startX: number;
    startY: number;
    speed: number;
    size: number;
    dispose: () => void;
}

export interface particleDrawProps {
    boundX: number;
    boundY: number;
}

export default class Particle {
    private p5: p5Types;
    private x: number;
    private y: number;
    private speed: number;
    private color: p5Types.Color;
    private size: number;
    private dispose: () => void;

    // Next movement direction in degrees
    private direction: number;
    private initDirection: number;

    constructor(props: particleProps) {
        this.x = props.startX;
        this.y = props.startY;
        this.p5 = props.p5;
        this.speed = props.speed;
        this.direction = this.p5.random(0, 2 * Math.PI);
        this.initDirection = this.direction;
        this.size = props.size;
        this.dispose = props.dispose;

        // Random colour from red to blue
        this.color = this.p5.color(
            this.p5.random(0, 255),
            0,
            this.p5.random(0, 255),
            150
        );
    }

    public draw(props: particleDrawProps) {
        // If the particle is out of bounds, call dispose() so we can be regenerated
        if (this.x > props.boundX + this.size || this.x < 0-this.size
            || this.y > props.boundY + this.size || this.y < 0-this.size) {
            this.dispose();
            return;
        }

        // Draw the particle

        // Set the color of the particle
        this.p5.fill(this.color);

        this.p5.ellipse(this.x, this.y, this.size, this.size);
        // Find the location x degrees from the current location
        this.x = this.x + this.speed * Math.cos(this.direction);
        this.y = this.y + this.speed * Math.sin(this.direction);
    }
}