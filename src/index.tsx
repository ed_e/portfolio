// Entry point for the application.

import { ThemeProvider } from '@emotion/react';
import { CssBaseline } from '@mui/material';
import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import Portfolio from './Portfolio';
import muiTheme from './theme';

ReactDOM.render(
    <ThemeProvider theme={muiTheme}>
        <CssBaseline />
        <Portfolio />
    </ThemeProvider>,
    document.getElementById('app-root'),
);